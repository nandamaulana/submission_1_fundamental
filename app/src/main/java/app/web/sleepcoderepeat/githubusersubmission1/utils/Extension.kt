package app.web.sleepcoderepeat.githubusersubmission1.utils

import kotlin.math.floor

fun Int.toKFormat() : String {
    var value = this.toString()
    if (this >=1000){
        value = "${floor((this/1000).toDouble()).toInt()}K"
    }
    return value
}