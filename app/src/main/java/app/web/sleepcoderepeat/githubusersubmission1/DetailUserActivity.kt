package app.web.sleepcoderepeat.githubusersubmission1

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import app.web.sleepcoderepeat.githubusersubmission1.model.UserModel
import app.web.sleepcoderepeat.githubusersubmission1.utils.Constants.Companion.DATA_USER_KEY
import app.web.sleepcoderepeat.githubusersubmission1.utils.toKFormat
import kotlinx.android.synthetic.main.activity_detail_user.*

class DetailUserActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_user)

        title = "Detail User"
        supportActionBar?.let {
            it.setHomeButtonEnabled(true)
            it.setDisplayHomeAsUpEnabled(true)
        }

        val dataUser = intent.getParcelableExtra<UserModel>(DATA_USER_KEY)

        img_user.setImageResource(dataUser.avatar)
        txt_nama.text = dataUser?.name
        txt_username.text = dataUser?.username
        txt_follower.text = getString(
            R.string.follower_following_format,
            dataUser?.follower?.toKFormat(),
            dataUser?.following?.toKFormat()
        )
        txt_repository.text = getString(R.string.repository_format, dataUser?.repository?.toKFormat())
        txt_company.text = dataUser?.company
        txt_location.text = dataUser?.location
    }
}
