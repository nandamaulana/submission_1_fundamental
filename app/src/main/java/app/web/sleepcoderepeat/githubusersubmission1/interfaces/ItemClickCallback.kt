package app.web.sleepcoderepeat.githubusersubmission1.interfaces

import android.view.View

interface ItemClickCallback {
    fun onItemClick(view: View, position: Int)
}