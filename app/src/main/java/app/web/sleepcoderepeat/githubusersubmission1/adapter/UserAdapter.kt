package app.web.sleepcoderepeat.githubusersubmission1.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubusersubmission1.R
import app.web.sleepcoderepeat.githubusersubmission1.interfaces.ItemClickCallback
import app.web.sleepcoderepeat.githubusersubmission1.model.UserModel
import app.web.sleepcoderepeat.githubusersubmission1.utils.toKFormat
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.list_item.view.*

class UserAdapter(
    private val listUser: ArrayList<UserModel>,
    private val listener: ItemClickCallback
) :
    RecyclerView.Adapter<UserAdapter.UserViewHolder>() {

    inner class UserViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cardView: CardView = itemView.card_view
        val imgUser: CircleImageView = itemView.img_user
        val txtNama: TextView = itemView.txt_nama
        val txtRepository: TextView = itemView.txt_repository
        val txtFollower: TextView = itemView.txt_follower
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder =
        UserViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false)
        )

    override fun getItemCount(): Int = listUser.size

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        val dataUser = listUser[position]
        holder.cardView.setOnClickListener {
            listener.onItemClick(it, position)
        }
        holder.imgUser.setImageResource(dataUser.avatar)
        holder.txtNama.text = dataUser.name
        holder.txtRepository.let {
            it.text =
                it.context.getString(R.string.repository_format, dataUser.repository.toKFormat())
        }
        holder.txtFollower.let {
            it.text = it.context.getString(R.string.follower_format, dataUser.follower.toKFormat())

        }

    }
}