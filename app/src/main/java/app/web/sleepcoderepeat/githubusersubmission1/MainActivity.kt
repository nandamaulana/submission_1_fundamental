package app.web.sleepcoderepeat.githubusersubmission1

import android.content.Intent
import android.content.res.TypedArray
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import app.web.sleepcoderepeat.githubusersubmission1.adapter.UserAdapter
import app.web.sleepcoderepeat.githubusersubmission1.interfaces.ItemClickCallback
import app.web.sleepcoderepeat.githubusersubmission1.model.UserModel
import app.web.sleepcoderepeat.githubusersubmission1.utils.Constants.Companion.DATA_FOLLOWER_KEY
import app.web.sleepcoderepeat.githubusersubmission1.utils.Constants.Companion.DATA_NAMA_KEY
import app.web.sleepcoderepeat.githubusersubmission1.utils.Constants.Companion.DATA_REPOSITORY_KEY
import app.web.sleepcoderepeat.githubusersubmission1.utils.Constants.Companion.DATA_USER_KEY
import app.web.sleepcoderepeat.githubusersubmission1.utils.Constants.Companion.IMG_USER_KEY

class MainActivity : AppCompatActivity(), ItemClickCallback{
    lateinit var username: Array<String>
    lateinit var name: Array<String>
    lateinit var avatar: TypedArray
    lateinit var company: Array<String>
    lateinit var location: Array<String>
    lateinit var repository: IntArray
    lateinit var follower: IntArray
    lateinit var following: IntArray

    private val getDataUser: ArrayList<UserModel>
        get() {
            val tempList = arrayListOf<UserModel>()
            for (position in name.indices) {
                tempList.add(
                    UserModel(
                        username[position],
                        name[position],
                        avatar.getResourceId(position, -1),
                        company[position],
                        location[position],
                        repository[position],
                        follower[position],
                        following[position]
                    )
                )
            }
            return tempList
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        prepare()
        showRecyclerList()
    }

    private fun prepare(){
        username = resources.getStringArray(R.array.username)
        name = resources.getStringArray(R.array.name)
        avatar = resources.obtainTypedArray(R.array.avatar)
        avatar.recycle()
        company = resources.getStringArray(R.array.company)
        location = resources.getStringArray(R.array.location)
        repository = resources.getIntArray(R.array.repository)
        follower = resources.getIntArray(R.array.follower)
        following = resources.getIntArray(R.array.following)
    }

    private fun showRecyclerList() {
        val adapter = UserAdapter(getDataUser,this)
        findViewById<RecyclerView>(R.id.rv_user).let {
            it.layoutManager = LinearLayoutManager(this)
            it.adapter = adapter
        }
        adapter.notifyDataSetChanged()
    }

    override fun onItemClick(view: View, position: Int) {
        val intent = Intent(this, DetailUserActivity::class.java)
        intent.putExtra(DATA_USER_KEY,  getDataUser[position])
        val option = ActivityOptionsCompat.makeSceneTransitionAnimation(
            this,
            Pair.create(view.findViewById(R.id.img_user),IMG_USER_KEY),
            Pair.create(view.findViewById(R.id.txt_nama),DATA_NAMA_KEY),
            Pair.create(view.findViewById(R.id.txt_repository),DATA_REPOSITORY_KEY),
            Pair.create(view.findViewById(R.id.txt_follower),DATA_FOLLOWER_KEY)
        ).toBundle()
        startActivity(intent, option)
    }
}
