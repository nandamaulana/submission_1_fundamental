package app.web.sleepcoderepeat.githubusersubmission1.utils

class Constants {
    companion object {
        const val DATA_USER_KEY = "data_user"
        const val IMG_USER_KEY = "img_user"
        const val DATA_NAMA_KEY = "txt_nama"
        const val DATA_REPOSITORY_KEY = "txt_repository"
        const val DATA_FOLLOWER_KEY = "txt_follower"
    }
}